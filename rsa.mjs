
import crypto from 'crypto'

export default class RSA {
  constructor() {
    this.chunk_size = 128;
    if (arguments.length == 1) {
      this.public_key = arguments[0];
    } else if (arguments.length == 2) {
      const keys = crypto.generateKeyPairSync('rsa', {
        modulusLength: arguments[0],
        publicKeyEncoding: {
          type: 'spki',
          format: 'pem'
        },
        privateKeyEncoding: {
          type: 'pkcs8',
          format: 'pem',
          cipher: 'aes-128-cbc',
          passphrase: arguments[1] 
        }
      });
      this.private_key = keys.privateKey;
      this.public_key = keys.publicKey;
    } else {
      console.error(new Error("Invalid number of arguments"));
    }
  }

  decrypt(hex_data, passphrase) {
    if (this.private_key) {
      const data = Uint8Array.from(Buffer.from(decodeURIComponent(hex_data), "base64"));

      const chunk_size = this.chunk_size;
      const chunks = [];
      for (let i = 0, j = data.length; i < j; i += chunk_size) {
        chunks.push(data.slice(i, i + chunk_size));
      }

      let decrypted_data = "";
      for (let c = 0; c < chunks.length; c++) {
        try {
          decrypted_data += crypto.privateDecrypt({
            key: this.private_key,
            passphrase: passphrase
          }, chunks[c]).toString("utf8");
        } catch (de) {
          console.error(de.stack);
        }
      }
      return decrypted_data;
    } else {
      console.error(new Error("Cannot decrypt without a private key!"));
      return undefined;
    }
  }

  encrypt(data) {
    if (typeof data == "object") data = JSON.stringify(data);

    const chunk_size = this.chunk_size;
    const chunks = data.match(new RegExp(`.{1,${chunk_size-42}}`, 'g'));

    const encd = new Uint8Array(
      chunk_size * chunks.length
    );
    for (let c = 0; c < chunks.length; c++) {
      const data_index = c*chunk_size;
      let chunk = undefined;
      try {
        chunk = crypto.publicEncrypt({ key: this.public_key }, Buffer.from(chunks[c]));
      } catch (en) {
        console.error(en.stack);
    
      }
      
      encd.set(chunk, data_index);
    }

    return encodeURIComponent(Buffer.from(encd).toString("base64"));
  }
}
