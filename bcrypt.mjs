
// NOTE: this module mimics bcrypt modules functionality in order to provide.
// Most of the code is GPT generated so need to be reviewed and well tested.
// Use only if both hashing and comparison is used on the broser.

import crypto from 'crypto'

export default class bcrypt {
  static base64_encode(buffer) {
    const base64_chars = './ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let encoded = '';
    let byte1, byte2, byte3;

    for (let i = 0; i < buffer.length; i += 3) {
      byte1 = buffer[i];
      byte2 = i + 1 < buffer.length ? buffer[i + 1] : 0;
      byte3 = i + 2 < buffer.length ? buffer[i + 2] : 0;

      encoded += base64_chars[byte1 >> 2];
      encoded += base64_chars[((byte1 & 0x03) << 4) | (byte2 >> 4)];
      if (i + 1 < buffer.length) {
        encoded += base64_chars[((byte2 & 0x0f) << 2) | (byte3 >> 6)];
      } else {
        encoded += base64_chars[(byte2 & 0x0f) << 2];
      }
      if (i + 2 < buffer.length) {
        encoded += base64_chars[byte3 & 0x3f];
      } else {
        encoded += '=';
      }
    }

    return encoded.slice(0, 22);
  }

  static gen_salt(rounds = 10) {
    const random_bytes = crypto.randomBytes(16);
    const base64_encoded = bcrypt.base64_encode(random_bytes);
    return `$2b$${rounds.toString().padStart(2, '0')}$${base64_encoded}`;
  }

  static hash_password(password, salt, rounds = 10) {
    const iterations = Math.pow(2, rounds);
    const hash_length = 32;

    const derivedKey = crypto.pbkdf2Sync(password, salt, iterations, hash_length, 'sha256');
    return `${salt}$${iterations}$${bcrypt.base64_encode(derivedKey)}`;
  }

  static compare_password(plain_password, hashed_password) {
    const [salt, iterations, hashed] = hashed_password.split('$');
    const derived_key_length = hashed.length / 2;

    const computed_hash = crypto.pbkdf2Sync(plain_password, salt, parseInt(iterations), derived_key_length, 'sha256').toString('hex');
    return `${salt}$${iterations}$${computed_hash}` === hashed_password;
  }
}
