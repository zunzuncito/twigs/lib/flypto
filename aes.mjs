
import crypto from 'crypto'

export default class AES {
  constructor(key) {
    this.key = key || crypto.randomBytes(32).toString("base64");
  }

  decrypt(data) {
    const key = Buffer.from(this.key, "base64");

    const iv = Buffer.from(data.substring(0, 24), "base64");
    const auth_tag = Buffer.from(data.substring(24, 48), "base64");
    const encrypted = data.substring(48);

    const decipher = crypto.createDecipheriv('aes-256-gcm', key, iv);
    decipher.setAuthTag(auth_tag);
    let str = decipher.update(encrypted, 'base64', 'utf8');
    str += decipher.final('utf8');
    return str;
  }

  encrypt(data) {
    if (typeof data == "object") data = JSON.stringify(data);
    const key = Buffer.from(this.key, "base64");

    const iv = crypto.randomBytes(16);
    const cipher = crypto.createCipheriv('aes-256-gcm', key, iv);

    let encrypted = cipher.update(data, 'utf8', 'base64');
    encrypted += cipher.final('base64');
    return iv.toString("base64")+cipher.getAuthTag().toString("base64")+encrypted;
  }

  static derive_key(password, salt, iterations) {
    salt = Buffer.from(salt, "base64");
    if (typeof iterations !== "number") iterations = 100000;
    const key_len = 32;
    return crypto.pbkdf2Sync(password, salt, iterations, key_len, 'sha512').toString("base64");
  }

  static key_sum(key) {
    let sum = 0;
    for (let c = 0; c < key.length; c++) {
      const cc = key.charCodeAt(c);
      sum += cc + cc * c + cc % (c+1) + cc / (c+1);
    }
    return Math.round(sum);
  }
}
