
import crypto from 'crypto';

export default class AES128 {
  constructor(key) {
    this.key = key || crypto.randomBytes(16).toString("base64");
  }

  decrypt(data) {
    const key = Buffer.from(this.key, "base64");

    const iv = Buffer.from(data.substring(0, 16), "base64");
    const auth_tag = Buffer.from(data.substring(16, 40), "base64");
    const encrypted = data.substring(40);

    const decipher = crypto.createDecipheriv('aes-128-gcm', key, iv);
    decipher.setAuthTag(auth_tag);
    let str = decipher.update(encrypted, 'base64', 'utf8');
    str += decipher.final('utf8');
    return str;
  }

  encrypt(data) {
    if (typeof data == "object") data = JSON.stringify(data);
    const key = Buffer.from(this.key, "base64");

    const iv = crypto.randomBytes(12); // 12 bytes for AES-128
    const cipher = crypto.createCipheriv('aes-128-gcm', key, iv);

    let encrypted = cipher.update(data, 'utf8', 'base64');
    encrypted += cipher.final('base64');
    return iv.toString("base64")+cipher.getAuthTag().toString("base64")+encrypted;
  }

  static derive_key(password, salt, iterations) {
    salt = Buffer.from(salt, "base64");
    if (typeof iterations !== "number") iterations = 100000;
    const key_len = 16;
    return crypto.pbkdf2Sync(password, salt, iterations, key_len, 'sha512').toString("base64");
  }

  static key_sum(key) {
    let sum = 0;
    for (let c = 0; c < key.length; c++) {
      const cc = key.charCodeAt(c);
      sum += cc + cc * c + cc % (c+1) + cc / (c+1);
    }
    return Math.round(sum);
  }
}

