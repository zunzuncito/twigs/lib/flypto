
# Encryption library `flypto`

## AES
```
  import AES from 'zunzun/flypto/aes.mjs'
```

### constructor(key)

### decrypt(data)

### encrypt(data)

### seeded_key(seed)

### key_sum(key)


## RSA
```
  import RSA from 'zunzun/flypto/rsa.mjs'
```

### constructor()

### decrypt(hex_data, passphrase)

### encrypt(data)
